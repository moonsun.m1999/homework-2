#EX 1:
def vdmot(a,b):
      x = a+b
      print(x)
  
vdmot(2,3) 


#ex3
def vdba():
  a = 8
print(a)

#biến được khai báo trong hàm chỉ được xác định khi hàm được gọi tên, nếu gọi ngoài hàm hệ thôn

#ex 4
def vdbon(c=12):
  return c

print(c+12)

#ex 5
a = 7
def vdnam(a):
  a = 8
print(a)

#Trường hợp này vì 'a' đã được khai báo trước đó nên không bị lỗi như vd 3